const axios = require("axios").default

/**
 * Network Request return
 * @typedef {Object} Response
 * @property {number|undefined} statusCode - Request status code
 * @property {string|undefined} statusMessage - Request status message
 * @property {string|undefined} message - Request message
 * @property {boolen} success - Request success (true | false)
 */


/** Class network call. */
class Network {
    /**
    * Request Options Object.
    * @param {Object} options - Network call options
    * @param {string} options.url - Request url
    * @param {string} options.method - Request method
    * @param {Object} options.headers - Request Headers
    * @param {string} options.headers.contentType - Request body type
    * @param {string} options.headers.Accept - Response type
    * @param {string} options.headers.Authorization - Authorization (JWT, Bearer, BasicAuth)
    */
    constructor(options) {
        this.options = options;
    }

    /**
    * Return response object.
    * @return {Response} options - Network call options
    */
    async request() {
        try {
            const response = await axios(this.options);
            return {
                statusCode: response.statusCode,
                statusMessage: response.statusMessage,
                success: true
            }
        } catch (error) {
            return {
                statusCode: null,
                statusMessage: null,
                message: error.message,
                success: false
            }
        }

    }
}

module.exports = Network;