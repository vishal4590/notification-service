const express = require("express")
const { v4: uuidv4 } = require('uuid');
const app = express();
const port = process.env.PORT || 3000
require("dotenv").config()

// Logger Service
const Logger = require("./core/logger");

// Providers
const { Main, Slack, Email, Twilio } = require("./providers");

/** Body Parser for Request Body */

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

/** Send Notification to Single User */
app.post("/send", (req, res, next) => {
    const id = uuidv4()
    const logger = new Logger({ name: "Main", id: id })
    try {
        // Normal Message Send
        // Message Payload
        const payload = {
            sender: req.body.sender,
            receiver: req.body.receiver,
            message: req.body.message
        }
        // new Main(payload, id).send()

        // Slack Message Send
        // new Slack(payload, id).send()

        // Twilio Message Send
        new Twilio(payload, id).send()
    } catch (error) {
        logger.error(error.message)
    }

    res.send("Message Sent");

})

/** Schedule Notification to Multiple Users */
app.post("/schedule", (req, res, next) => {
})

app.listen(port, () => {
    console.log(`Server running on ${port}`)
})



