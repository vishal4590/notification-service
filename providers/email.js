const Main = require("./main")

class Email extends Main {
    /**
    * Email Object.
    * @param {Object} payload - Main message object
    * @param {string} payload.sender - Sender id
    * @param {string} payload.receiver - Receiver id
    * @param {string} payload.message - Message Content
    * @param {id} - Unique id for message track
    */
    constructor(payload, id) {
        super(payload, id)
        this.payload = payload
        this.payload.name = "Email"
    }
}


module.exports = Email