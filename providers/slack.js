const Main = require("./main")
class Slack extends Main {
    /**
    * Slack Object.
    * @param {Object} payload - Main message object
    * @param {string} payload.sender - Sender id
    * @param {string} payload.receiver - Receiver id
    * @param {string} payload.message - Message Content
    * @param {id} - Unique id for message track
    */
    constructor(payload, id) {
        super(payload, id)
        this.payload = payload
        this.payload.name = "Slack"
    }
}
module.exports = Slack