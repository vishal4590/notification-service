
// Logger Service
const Logger = require("../core/logger");
const Network = require("../core/network");

/** List of providers */
const Providers = { "EMAIL": 1, "SLACK": 2, "FIREBASE": 3, "TWILIO": 4 }
Object.freeze(Providers)


/** Class representing a main message class. */
class Message {
    /**
     * Message Object.
     * @param {Object} payload - Main message object
     * @param {string} payload.sender - Sender id
     * @param {string} payload.receiver - Receiver id
     * @param {string} payload.message - Message Content
     * @param {id} - Unique id for message track
     */
    constructor(payload, id, options) {
        this.payload = payload
        this.options = options
        this.id = id
    }

    /**
    * Async send function
    */
    async send() {

        const logger = new Logger({ name: "APINotification", id: this.id })
        logger.setLogData(this.payload)
        logger.info("Sending Message via provider api")

        /** Common API Call For Notification Send */
        const network = new Network(this.options)
        const result = await network.request()

        /** Check API Response and logs error or success */
        if (result.success) {
            logger.info(result.message)
        } else {
            logger.error(result.message)
        }

    }
}


module.exports = Message