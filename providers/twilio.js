
// Logger Service
const Logger = require("../core/logger");
const { v4: uuidv4 } = require('uuid');
const qs = require("qs")
const Main = require("./main")

/** Twilio Message API Options Parameters */

const options = {
    url: `${process.env.TWILIO_API}/${process.env.TWILIO_ACCOUNT}/Messages`,
    method: "POST",
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        "Authorization": `Basic ${Buffer.from(process.env.TWILIO_ACCOUNT + ":" + process.env.TWILIO_AUTH).toString('base64')}`
    }
}

/** Class for Twilio Provider. */
class Twilio extends Main {
    /**
     * Message Object.
     * @param {Object} payload - Main message object
     * @param {string} payload.receiver - Receiver id
     * @param {string} payload.message - Message Content
     * @param {id} - Unique id for message track
     */
    constructor(payload, id) {
        options.data = qs.stringify({
            "To": payload.receiver,
            "MessagingServiceSid": process.env.TWILIO_MESSAGE_ID,
            "Body": payload.message,
        })
        super(payload, id, options)
        this.payload.name = "Twilio"
    }
}


module.exports = Twilio